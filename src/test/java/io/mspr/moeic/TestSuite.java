package io.mspr.moeic;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TestSuite {

	public WebDriver driver;

	@Before
	public void setUp() {
			System.out.println("########################## Execution in GitLab Started #########################");
			System.out.println("Automation Script Running on " + System.getProperty("os.name"));

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--test-type");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--headless");
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + new File("/chromedriver"));
			driver = new ChromeDriver(options);
			System.out.println("Automation Driver is " + driver);
		}

	@Test
	public void titleTest() {
		System.out.println("########################## Test Case Execution Started #########################");
		driver.get("http://104.155.24.223/");
		assertEquals("Application de test", driver.getTitle());
		System.out.println("########################## Test Case Execution Completed #########################");
	}
	
	@Test
	public void baladeTest() {
		System.out.println("########################## Test Case Execution Started #########################");
		driver.get("http://104.155.24.223/");
	    driver.findElement(By.linkText("Page de test")).click();
	    driver.findElement(By.linkText("A propos")).click();
	    driver.findElement(By.linkText("Application de test")).click();
		System.out.println("########################## Test Case Execution Completed #########################");
	}

		@After
		public void tearDown() {
			driver.quit();
		}
}
