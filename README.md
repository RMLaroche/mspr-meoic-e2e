# Projet d'exemple pour le MSPR : MEO Integration Continue

## Objectif
L'objectif de cette mission est de mettre on oeuvre une architecture d'intégration continue.

## Membres du groupe 
- Vianney Chepeau
- Yann Hary
- Remy Laroche
- Nianarinjara Ratrimoarison

# Description de l'application

L'application contient simplement une servlet permettant d'afficher une page web.

# IP des serveurs
- IP du serveur de Prod : 35.187.1.237
- IP du serveur de Pre-prod : 104.155.24.223
